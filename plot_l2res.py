#!/usr/bin/env python

# Histogram post process                                                                                                                                         
# 28.4.2023/S.Lehti                                                                                                                                      
#

import os,sys,re
import subprocess

import ROOT

from optparse import OptionParser

from plot_v2 import xsecs,Dataset,getDatasetsMany,read,mergeDatasets,mergeExtDatasets,normalizeToLumi,reorderDatasets,writeCounters

ROOT.gROOT.SetBatch(True)
ROOT.gErrorIgnoreLevel = ROOT.kWarning

root_re = re.compile("(?P<rootfile>([^/]*histograms.root))")

def usage():
    print()
    print("### Usage:  ",os.path.basename(sys.argv[0]),"<multicrab histograms>")
    print()

def main(opts, args):

    if len(sys.argv) == 1:
        usage()
        sys.exit()

    multicrabdirs = args

    for multicrabdir in multicrabdirs:
        if os.path.exists(multicrabdir) and os.path.isdir(multicrabdir):
            print(multicrabdir)
        else:
            usage()
            sys.exit()

    whitelist = opts.includeTasks
    blacklist = opts.excludeTasks
    print("Getting datasets")
    datasets = getDatasetsMany(multicrabdirs,whitelist=whitelist,blacklist=blacklist)
    print("Datasets received")

    rootdir = "l2res/"
    datasets = read(datasets,rootdir)
    #datasets = mergeExtDatasets(datasets)
    datasets = normalizeToLumi(datasets)
    ####writeCounters(datasets)
    datasets = mergeDatasets("Data","_Run20\d\d\S_",datasets)

    datasets = mergeDatasets("MC","^(?!Data).*",datasets)
    print("Merged MC datasets")

    datasets = reorderDatasets(datasets)
    print("datasets reordered")

    for d in datasets:
        print(d.name,d.isData())

    #fOUT = ROOT.TFile.Open("l2res.root","RECREATE")
    #fOUT.cd()

    histogramNames = datasets[0].histo.keys()
    for d in datasets:
        newdir = "Zjet"
        data = "data"
        if not d.isData():
            histogramNames = d.histo.keys()
            data = "mc"
        fOUT = ROOT.TFile.Open("jmenano_%s_Zjet.root"%data,"RECREATE")
        fOUT.cd()
        nd = fOUT.mkdir(newdir)
        nd.cd()
        for h in histogramNames:
            histo = d.histo[h]
            newname = h.replace(rootdir,"")
            histo.SetName(newname)
            histo.Write()
        fOUT.Close()
    
    print("Workdir:",os.getcwd())

if __name__ == "__main__":

    parser = OptionParser(usage="Usage: %prog [options]")
    parser.add_option("-i", "--includeTasks", dest="includeTasks", default="None", type="string",
                      help="Only perform action for this dataset(s) [default: \"\"]")

    parser.add_option("-e", "--excludeTasks", dest="excludeTasks", default="None", type="string",
                      help="Exclude this dataset(s) from action [default: \"\"]")

    (opts, args) = parser.parse_args()

    opts.includeTasks = opts.includeTasks.split(',')
    opts.excludeTasks = opts.excludeTasks.split(',')
    if 'None' in opts.includeTasks:
        opts.includeTasks = []
    if 'None' in opts.excludeTasks:
        opts.excludeTasks = []

    main(opts, args)
