#! /usr/bin/python
import os
import argparse
import time
import glob

IOV_list = [
    # 2023
    "2023C_22Sep2023_v1",
    "2023C_22Sep2023_v2",
    "2023C_22Sep2023_v3",
    "DYJetsToLL_M_50",
    "2023C_22Sep2023_v4",
    "2023D",
    "DYJetsToLL_M_50_postBPix",
    # 2022
    "Muon_Run2022C_22Sep2023_v1",
    "Muon_Run2022D_22Sep2023_v1",
    "DYJetsToLL_M_50_Summer22BCD",
    "Muon_Run2022E_22Sep2023_v1",
    "Muon_Run2022F_22Sep2023_v2",
    "Muon_Run2022G_22Sep2023_v1",
    "DYJetsToLL_M_50_Summer22EF",
]
# IOV_list = [
#     "Muon_Run2022F_22Sep2023_v2",
# ]

# resources for slurm
res_iovs = {
    # dataset: [memory, hours]
    "2023C_22Sep2023_v1": [13, 2],
    "2023C_22Sep2023_v2": [5, 1],
    "2023C_22Sep2023_v3": [6, 1],
    "DYJetsToLL_M_50": [55, 3],
    "2023C_22Sep2023_v4": [20, 3],
    "2023D": [20, 3],
    "DYJetsToLL_M_50_postBPix": [30, 3],

    "Muon_Run2022C_22Sep2023_v1": [30, 5],
    "Muon_Run2022D_22Sep2023_v1": [15, 2],
    "DYJetsToLL_M_50_Summer22BCD": [15, 3],
    "Muon_Run2022E_22Sep2023_v1": [6, 2],
    "Muon_Run2022F_22Sep2023_v2": [45, 5],
    "Muon_Run2022G_22Sep2023_v1": [10, 2],
    "DYJetsToLL_M_50_Summer22EF": [50, 5],
}

run2023C123 = [
    x for x in IOV_list if ("2023C" in x and "v4" not in x) or x == "DYJetsToLL_M_50"
]
run2023C4 = [x for x in IOV_list if "2023C" in x and "v4" in x]
run2023D = [x for x in IOV_list if "2023D" in x or x == "DYJetsToLL_M_50_postBPix"]
run3_23 = run2023C123 + run2023C4 + run2023D
run3_22 = [x for x in IOV_list if ("2022" in x or "Summer22" in x)]


parser = argparse.ArgumentParser(description="Run all IOVs")

parser.add_argument("-i", "--IOV_list", required=True, type=str, nargs="+")
parser.add_argument("-v", "--version", required=True)
parser.add_argument("-l", "--local", default=False, action="store_true", help="Run locally in the background")
parser.add_argument("-d", "--debug", default=False, action="store_true", help="Run locally printing the log")
parser.add_argument("-m", "--max_files", default=-1)
parser.add_argument("-p", "--pnetreg", default=False, action="store_true")
parser.add_argument("-n", "--neutrino", default=False, action="store_true")
parser.add_argument("-c", "--closure", default=False, action="store_true")
parser.add_argument("-f", "--fast", default=False, action="store_true")
parser.add_argument("-of", "--only-failed", default=False, action="store_true")
args = parser.parse_args()

IOV_input = []
if args.IOV_list:
    if "all" in args.IOV_list:
        IOV_input = IOV_list
    elif "run2023C123" in args.IOV_list:
        IOV_input = run2023C123
    elif "run2023C4" in args.IOV_list:
        IOV_input = run2023C4
    elif "run2023D" in args.IOV_list:
        IOV_input = run2023D
    elif "23" in args.IOV_list and args.IOV_list[0].isdigit():
        IOV_input = run3_23
    elif "22" in args.IOV_list and args.IOV_list[0].isdigit():
        IOV_input = run3_22
    else:
        # Check that all IOVs passed are in the list
        for iov in args.IOV_list:
            if iov not in IOV_list:
                print("IOV " + iov + " not in list of IOVs")
                exit()
            else:
                IOV_input.append(iov)
else:
    print("No IOV list passed")
    exit()

if (args.version) and ("test" not in args.IOV_list):
    version = args.version

if args.max_files and ("test" not in args.IOV_list):
    max_files = args.max_files

if args.only_failed:
    # check the size of IOV root file
    print("Total IOVs: ", IOV_input)
    IOV_input_failed=[]

    histo_main_dir_prefix=f"rootfiles/{version}"
    #get all directories with this prefix using glob
    all_histo_dirs = glob.glob(histo_main_dir_prefix + "*")
    for iov in IOV_input:
        histo_dir = [x for x in all_histo_dirs if iov in x][0]
        # look recursively inside the directory until find a root file
        file_names=[]
        for root, dirs, files in os.walk(histo_dir):
            for file in files:
                if file.endswith(".root"):
                    file_names.append(os.path.join(root, file))
        for file_name in file_names:
            if os.path.exists(file_name):
                size = os.path.getsize(file_name)
                print(f"Checking IOV {iov} in file {file_name}")
                if size < 2000:
                    print(f"IOV {iov} has size {size} bytes, will rerun")
                    IOV_input_failed.append(iov)
                    break
                else:
                    print(f"IOV {iov} has size {size} bytes, will not rerun")
            else:
                print(f"IOV {iov} does not exist, will rerun")
                IOV_input_failed.append(iov)

    IOV_input=IOV_input_failed

print("IOVs to run: ", IOV_input, len(IOV_input))

if not os.path.exists("/work/mmalucch/logs_L2L3Res/zb_logs/" + version):
    os.makedirs("/work/mmalucch/logs_L2L3Res/zb_logs/" + version)

pnetreg = args.pnetreg
if "pnetreg" in version:
    pnetreg = True

neutrino = args.neutrino
if "neutrino" in version:
    neutrino = True

closure = args.closure
if "closure" in version:
    closure = True


iov_string = ",".join(IOV_input)
print("IOV string: ", iov_string)
# for iov in IOV_input:
print(f"Running IOV {IOV_input} with version {version}")

if not args.fast:

    # choose is pnetreg or pnetregneutrino
    with open("src/Analysis.cc", "r") as file:
        filedata = file.read()

    if pnetreg and not neutrino:
        print("Setting up PNetReg without neutrino")
        if "// #define PNETREG\n" in filedata:
            print("uncommenting PNETREG")
            filedata = filedata.replace("// #define PNETREG\n", "#define PNETREG\n")
        if not "// #define PNETREGNEUTRINO\n" in filedata:
            print("commenting PNETREGNEUTRINO")
            filedata = filedata.replace(
                "#define PNETREGNEUTRINO\n", "// #define PNETREGNEUTRINO\n"
            )
    elif pnetreg and neutrino:
        print("Setting up PNetReg with neutrino")
        if "// #define PNETREGNEUTRINO\n" in filedata:
            print("uncommenting PNETREGNEUTRINO")
            filedata = filedata.replace(
                "// #define PNETREGNEUTRINO\n", "#define PNETREGNEUTRINO\n"
            )
        if not "// #define PNETREG\n" in filedata:
            print("commenting PNETREG")
            filedata = filedata.replace("#define PNETREG\n", "// #define PNETREG\n")
    else:
        print("Using standard jet pT")
        if not "// #define PNETREG\n" in filedata:
            print("commenting PNETREG")
            filedata = filedata.replace("#define PNETREG\n", "// #define PNETREG\n")
        if not "// #define PNETREGNEUTRINO\n" in filedata:
            print("commenting PNETREGNEUTRINO")
            filedata = filedata.replace(
                "#define PNETREGNEUTRINO\n", "// #define PNETREGNEUTRINO\n"
            )

    # find line that starts with bool CLOSURE_L2L3RES
    for line in filedata.split("\n"):
        if line.startswith("bool CLOSURE_L2L3RES"):
            if closure:
                print("Setting CLOSURE_L2L3RES to true")
                line_new = f"bool CLOSURE_L2L3RES = true;"
            else:
                print("Setting CLOSURE_L2L3RES to false")
                line_new = f"bool CLOSURE_L2L3RES = false;"
            break
    # modify line
    filedata = filedata.replace(line, line_new)

    # print(filedata[:700])

    with open("src/Analysis.cc", "w") as file:
        file.write(filedata)

    time.sleep(10)

    # clean and make
    os.system("make clean")
    os.system("make")

multicrab_dict = {
    "23": "/work/mmalucch/multicrab_dirs_post_processing/multicrab_ZbAnalysis_v1331p1_Run2023BCD_20231221T0844",
    (
        "22B",
        "22C",
        "22D",
    ): "/work/mmalucch/multicrab_dirs_post_processing/multicrab_ZbAnalysis_v1407_Run2022CD_20240917T1647",
    (
        "22E",
        "22F",
        "22G",
    ): "/work/mmalucch/multicrab_dirs_post_processing/multicrab_ZbAnalysis_v1407_Run2022EFG_20240917T1651",
}


for iov in IOV_input:
    print(
        f"Running IOV {iov} with version {version}, memory {res_iovs[iov][0]} and time {res_iovs[iov][1]}"
    )

    multicrab = multicrab_dict["23"]
    for names, multicrab_opt in multicrab_dict.items():
        for name in names:
            if name in iov:
                multicrab = multicrab_opt
    print(f"Multicrab: {multicrab}")

    if args.local:
        os.system(
            f"nohup time python3 analyse.py {multicrab} -m {max_files} -i {iov} --name {version}_{iov} > /work/mmalucch/logs_L2L3Res/zb_logs/{version}/log_{iov}_{version}.log &"
        )
    elif args.debug:
        os.system(
            f"time python3 analyse.py {multicrab} -m {max_files} -i {iov} --name {version}_{iov}"
        )
    else:
        os.system(
            f"sbatch --time=0{res_iovs[iov][1]}:00:00 --mem={res_iovs[iov][0]}gb --job-name=zb_{iov}_{version} -p standard --account=t3 --ntasks=1  --cpus-per-task=8  --output=/work/mmalucch/logs_L2L3Res/zb_logs/{version}/log_{iov}_{version}.log submit_slurm.sh {iov} {version} {multicrab} {max_files}"
        )
    print(f" => Follow logging with 'tail -f /work/mmalucch/logs_L2L3Res/zb_logs/{version}/log_{iov}_{version}.log'")
