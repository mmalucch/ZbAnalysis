#!/usr/bin/env python

import os,sys,re
import subprocess
import json

from optparse import OptionParser

import LumiJsonTest

def execute(cmd):
    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE)
    f = p.stdout
    ret=[]
    for line in f:
        line = line.decode('utf-8')
        ret.append(line.replace("\n", ""))
    f.close()
    return ret

def fix(inputfile):
    with open(inputfile, 'r') as fIN:
        data = json.load(fIN)

    with open("temp.json", "w") as fOUT:
        json.dump(data, fOUT)

    cmd = '''sed -i 's/, "/,\\n"/g' temp.json'''
    #print(cmd)
    os.system(cmd)

    rename = 'mv temp.json %s'%(inputfile)
    os.system(rename)

def main(opts, args):

    jsonfiles = []
    for arg in args:
        if os.path.isfile(arg) and arg.startswith("Cert"):
            jsonfiles.append(arg)
        if os.path.isdir(arg):
            jsonfiles += execute('find %s -name "Cert*.*"'%arg)
    #print(jsonfiles)
    for jsonfile in jsonfiles:
        #print(jsonfile)
        passed = True
        with open(jsonfile, 'r') as fIN:
            data = json.load(fIN)
            #print(list(data.keys()))
            for key in list(data.keys()):
                for i in range(len(data[key])):
                    run = int(key)
                    lumi = data[key][i][0]
                    passed = passed and LumiJsonTest.passed(jsonfile,run,lumi)
            if passed:
                print(jsonfile,"\033[92mPASSED\033[0m")
            else:
                print(jsonfile,"\033[91mNOT PASSING\033[0m")
                if opts.fix:
                    fix(jsonfile)
                
                    

if __name__=="__main__":
    parser = OptionParser(usage="Usage: %prog [options]")
    parser.add_option("--fix", dest="fix", default=False, action="store_true",
                      help="Fix the lumijson file [default: 'False']")
    (opts, args) = parser.parse_args()

    main(opts, args)
