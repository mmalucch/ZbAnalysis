#!/bin/bash


#/ # SBATCH --job-name=zb_analysis         # Job name
#/ # SBATCH -p standard
#/ # SBATCH --account=t3
#/ # SBATCH --ntasks=1                          # Run a single task
#/ # SBATCH --cpus-per-task=8                   # Number of CPU cores per task; Example of  submitting  8-core parallel SMP job
#/ # SBATCH --output=logs/$2/zb_analysis_$1_$2_%j.log                 # Standard output and error log



# python runIOVs.py -i run2023C123 -v new_tot_23_pnetreg
# python runIOVs.py -i run2023C4 -v new_tot_23_pnetreg
# python runIOVs.py -i run2023D -v new_tot_23_pnetreg
time python3 analyse.py $3 -m $4 -i $1 --name $2_$1

#/ #SBATCH --time=0$4:00:00                     # Time limit hrs:min:sec
#/ #SBATCH --mem=$3 gb                           # Job memory request
#/ #SBATCH -w t3wn58                           # choose particular Compute Node from wn partition
#/ #SBATCH --mem-per-cpu=3072                 # example of memory request for one CPU core