import os
import argparse

parser = argparse.ArgumentParser(description="Move files")
parser.add_argument("-v", "--version", required=True)
parser.add_argument("-i", "--IOV_list", default="all")
args = parser.parse_args()

prefix='rootfiles/'+args.version+'_'

eras_23=["2023C_22Sep2023_v123","2023C_22Sep2023_v4","2023D"]
eras_22=["Muon_Run2022CD_22Sep2023_v1", "Muon_Run2022E_22Sep2023_v1", "Muon_Run2022F_22Sep2023_v2", "Muon_Run2022G_22Sep2023_v1", "Muon_Run2022FG_22Sep2023"]

eras=[]

if "all" in args.IOV_list or "23" in args.IOV_list:
    print("2023")
    # 2023
    os.system(f"mkdir {prefix}2023C_22Sep2023_v123")
    os.system(f"cp -r  {prefix}2023C_22Sep2023_v1/* {prefix}2023C_22Sep2023_v2/* {prefix}2023C_22Sep2023_v3/* {prefix}2023C_22Sep2023_v123/")
    os.system(f"cp -r {prefix}DYJetsToLL_M_50/* {prefix}2023C_22Sep2023_v123")

    os.system(f"cp -r  {prefix}DYJetsToLL_M_50/* {prefix}2023C_22Sep2023_v4")

    os.system(f"cp -r  {prefix}DYJetsToLL_M_50_postBPix/* {prefix}2023D")
    eras+=eras_23

if "all" in args.IOV_list or "22" in args.IOV_list:
    print("2022")
    # 2022
    os.system(f"mkdir {prefix}Muon_Run2022CD_22Sep2023_v1")
    os.system(f"cp -r {prefix}Muon_Run2022C_22Sep2023_v1/* {prefix}Muon_Run2022D_22Sep2023_v1/* {prefix}Muon_Run2022CD_22Sep2023_v1/")
    os.system(f"cp -r {prefix}DYJetsToLL_M_50_Summer22BCD/DYJetsToLL_M_50_Summer22BCD/ {prefix}Muon_Run2022CD_22Sep2023_v1")


    os.system(f"mkdir {prefix}Muon_Run2022FG_22Sep2023")
    os.system(f"cp -r {prefix}DYJetsToLL_M_50_Summer22EF/* {prefix}Muon_Run2022E_22Sep2023_v1")
    os.system(f"cp -r {prefix}DYJetsToLL_M_50_Summer22EF/* {prefix}Muon_Run2022F_22Sep2023_v2")
    os.system(f"cp -r {prefix}DYJetsToLL_M_50_Summer22EF/* {prefix}Muon_Run2022G_22Sep2023_v1")
    os.system(f"cp -r {prefix}Muon_Run2022F_22Sep2023_v2/* {prefix}Muon_Run2022G_22Sep2023_v1/* {prefix}Muon_Run2022FG_22Sep2023")
    eras+=eras_22


for era in eras:
    print(f"\n plot {era}\n")
    os.system(f"python plot_v3.py {prefix}{era}")
