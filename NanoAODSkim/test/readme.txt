################################################
# NanoAOD skimming with trigger bit only
# 10.12.2019/SLehti
################################################


#https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookNanoAOD
git cms-addpkg CondFormats/JetMETObjects
git clone https://github.com/cms-nanoAOD/nanoAOD-tools.git PhysicsTools/NanoAODTools

git clone ssh://git@gitlab.cern.ch:7999/slehti/ZbAnalysis.git

################################################

Copy NanoAOD_*AnalysisSkim.py for your analysis name
and change the triggers in the code.

Change the input file and test interacively:
python NanoAOD_*AnalysisSkim.py

Submit:
./multicrab.py --create -n NanoAOD_ZbAnalysisSkim.py

After DATA is 100% complete:
lumicalc.py <multicrabdir>
pileup.py <multicrabdir>
