#ifndef Lumimask_h
#define Lumimask_h

#include <string>
#include <map>

class Lumimask {
 public:
  Lumimask(std::string filename,short verbose = 0);
  ~Lumimask();

  bool filter(uint run, uint lumi);
  void print(std::string message = "");

 private:
  std::string lumimaskfile;
  std::map<uint, std::map<uint,uint> > lumijson;
  std::map<uint, std::map<uint,uint> > processedlumi;

  void parse_line(std::string);
  std::string compress(uint arr[], size_t n);

};
#endif
