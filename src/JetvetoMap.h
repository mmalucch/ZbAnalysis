#ifndef JetvetoMap_h
#define JetvetoMap_h

#include "TFile.h"
#include "TH2D.h"
#include "TLorentzVector.h"
#include <string>

class JetvetoMap {
 public:
  JetvetoMap(std::string,bool);
  ~JetvetoMap();

  bool pass(TLorentzVector&);

 private:
  TH2D* map;
  bool useMap;
};
#endif
