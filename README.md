# ZbAnalysis

Z+jet analysis to obtain the inputs for the L2L3Residuals  within the JetMET group of CMS as described by the [JERC](https://cms-jerc.web.cern.ch/) sub-group. The results are to be combined with the [photon+jet](https://github.com/matteomalucchi/gamjet-analysis) and [dijet](https://github.com/matteomalucchi/dijet) analyses with the scripts provided in the [jecsys3](https://github.com/matteomalucchi/jecsys3) repository.

## Dependencies

An installation of ROOT is required to run the scripts.

## How to run

Run on slurm for 2022+2023 recompiling the libraries

```bash
python runIOVs.py -i all -v version
```

This script executes the following actions:

- comment / uncomment the `#define PNETREG` / `#define PNETREGNEUTRINO` in `src/Analysis.cc` to choose whether to apply the PNet regression or the PNet regression including neutrino
- `make clean`
- `make`

Once the jobs are completed, to do the post processing run:

```bash
python post_processing.py  -i all -v version
```

This script executes the following actions:

- merge together the output directories
- create the combined outputs by running `python plot_v3.py  multicrabdir`

## How to create the multicrab inputs
Follow the instructions in the [NanoAnalysis](https://gitlab.cern.ch/HPlus/NanoAnalysis/-/tree/master/NanoAODSkim/test?ref_type=heads) repository to create the multicrab inputs.