import awkward as ak
import numpy
import sys
import os
import re

def triggerSelection(events,year,leptonflavor):
    if leptonflavor == 11:
        return (events.HLT.Ele23_Ele12_CaloIdL_TrackIdL_IsoVL &
                events.HLT.Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ
        )
    if leptonflavor == 13:
        return events.HLT.Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ
    return None

def lumimask(events,lmask):
    return (lmask.passed(events))

def METCleaning(events,year):
    if '2016' in year:
        return (events.Flag.goodVertices &
                events.Flag.globalSuperTightHalo2016Filter &
                events.Flag.HBHENoiseFilter &
                events.Flag.HBHENoiseIsoFilter &
                events.Flag.BadPFMuonFilter &
                events.Flag.eeBadScFilter
        )

    return (events.Flag.goodVertices &
            events.Flag.globalSuperTightHalo2016Filter &
            events.Flag.HBHENoiseFilter &
            events.Flag.HBHENoiseIsoFilter &
            events.Flag.BadPFMuonFilter &
            events.Flag.ecalBadCalibFilter
    )

def leptonSelection(events, LEPTONFLAVOR):
    if LEPTONFLAVOR == 13:
        return (
            (ak.num(events.Muon) >= 2)
            & (ak.num(events.Muon) <= 3)
            & (ak.sum(events.Muon.charge, axis=1) <= 1)
            & (ak.sum(events.Muon.charge, axis=1) >= -1)
        )

    if LEPTONFLAVOR == 11:
        return (
            (ak.num(events.Electron) == 2)
            & (ak.num(events.Electron) <= 3)
            & (ak.sum(events.Electron.charge, axis=1) <= 1)
            & (ak.sum(events.Electron.charge, axis=1) >= -1)
        )
    return None

def Zboson(events,LEPTONFLAVOR):
    if LEPTONFLAVOR == 13:
        leptonsPlus = events.Muon[(events.Muon.charge == 1)]
        leptonsMinus= events.Muon[(events.Muon.charge == -1)]
    if LEPTONFLAVOR == 11:
        leptonsPlus = events.Electron[(events.Electron.charge == 1)]
        leptonsMinus= events.Electron[(events.Electron.charge == -1)]
    return leptonsPlus[:, 0] + leptonsMinus[:, 0]

def Zpt(Zboson,events):
    return (Zboson.pt > 15)

def jets(events):
    jet_cands = events.Jet

    leptons = ak.with_name(ak.concatenate([events.Muon, events.Electron], axis=1), 'PtEtaPhiMCandidate')
    jet_cands = drClean(jet_cands,leptons)
#    jet_cands = drClean(jet_cands,events.Muon)
    #jet_cands = smear_jets(events,jet_cands)
    jet_cands = JEC(events,jet_cands)
    
    return jet_cands

def smear_jets(events,jets):
    print("check smear1",jets.pt)
    print("check smear2",jets.pt)
    return jets

def drClean(coll1,coll2,cone=0.4):
    from coffea.nanoevents.methods import vector
    j_eta = coll1.eta
    j_phi = coll1.phi
    l_eta = coll2.eta
    l_phi = coll2.phi

    j_eta, l_eta = ak.unzip(ak.cartesian([j_eta, l_eta], nested=True))
    j_phi, l_phi = ak.unzip(ak.cartesian([j_phi, l_phi], nested=True))
    delta_eta = j_eta - l_eta
    delta_phi = vector._deltaphi_kernel(j_phi,l_phi)
    dr = numpy.hypot(delta_eta, delta_phi)
    jets_noleptons = coll1[~ak.any(dr < cone, axis=2)]
    return jets_noleptons

def JEC(events,jets):
    from coffea.lookup_tools import extractor
    from coffea.jetmet_tools import FactorizedJetCorrector, JetCorrectionUncertainty
    from coffea.jetmet_tools import JECStack, CorrectedJetsFactory
    import awkward as ak
    import numpy as np

    ext = extractor()
    ext.add_weight_sets([
        "* * data/Fall17_17Nov2017B_V32_DATA_L1FastJet_AK4PFchs.txt",
        "* * data/Fall17_17Nov2017B_V32_DATA_L2Relative_AK4PFchs.txt",
        "* * data/Fall17_17Nov2017B_V32_DATA_L3Absolute_AK4PFchs.txt",
        "* * data/Fall17_17Nov2017B_V32_DATA_L2L3Residual_AK4PFchs.txt",
    ])
    ext.finalize()

    jec_stack_names = [
        "Fall17_17Nov2017B_V32_DATA_L1FastJet_AK4PFchs",
        "Fall17_17Nov2017B_V32_DATA_L2Relative_AK4PFchs",
        "Fall17_17Nov2017B_V32_DATA_L3Absolute_AK4PFchs",
        "Fall17_17Nov2017B_V32_DATA_L2L3Residual_AK4PFchs",
    ]

    evaluator = ext.make_evaluator()

    jec_inputs = {name: evaluator[name] for name in jec_stack_names}
    jec_stack = JECStack(jec_inputs)

    name_map = jec_stack.blank_name_map
    name_map['JetPt'] = 'pt'
    name_map['JetMass'] = 'mass'
    name_map['JetEta'] = 'eta'
    name_map['JetA'] = 'area'

    jets = events.Jet

    jets['pt_raw'] = (1 - jets['rawFactor']) * jets['pt']
    jets['mass_raw'] = (1 - jets['rawFactor']) * jets['mass']
#    jets['pt_gen'] = ak.values_astype(ak.fill_none(jets.matched_gen.pt, 0), np.float32)
    jets['rho'] = ak.broadcast_arrays(events.fixedGridRhoFastjetAll, jets.pt)[0]
#    name_map['ptGenJet'] = 'pt_gen'
    name_map['ptRaw'] = 'pt_raw'
    name_map['massRaw'] = 'mass_raw'
    name_map['Rho'] = 'rho'

    events_cache = events.caches[0]

    jet_factory = CorrectedJetsFactory(name_map, jec_stack)
    corrected_jets = jet_factory.build(jets, lazy_cache=events_cache)

#    print('untransformed pts',jets.pt[0],jets.pt_raw[0])
#    print('transformed pts',corrected_jets.pt[0],corrected_jets.pt_raw[0])

    return corrected_jets

def recalculateMET(jets):
    return 0.

def plotResponce(events,jets,Zboson,out,eweight):
    """
    leadingJet = jets[:,0]
    R_pT = leadingJet.pt/Zboson.pt
    
    MET = recalculateMET(jets)

    METuncl = -MET - jets_all.pt - Zboson.pt
    R_MPF = 1 + MET.pt*(cos(MET.phi)*Zboson.Px() + sin(MET.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());
    R_MPFjet1 = -leadingJet.Pt()*(cos(leadingJet.Phi())*Zboson.Px() + sin(leadingJet.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());
    R_MPFjetn = -jets_notLeadingJet.Pt()*(cos(jets_notLeadingJet.Phi())*Zboson.Px() + sin(jets_notLeadingJet.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());
    R_MPFuncl = -METuncl.Pt()*(cos(METuncl.Phi())*Zboson.Px() + sin(METuncl.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());

              hprof2D_ZpT_Mu_RpT[i]->Fill(Zboson.Pt(),mu,R_pT,eweight);
          hprof2D_ZpT_Mu_RMPF[i]->Fill(Zboson.Pt(),mu,R_MPFPF,eweight);
          hprof2D_ZpT_Mu_RMPFjet1[i]->Fill(Zboson.Pt(),mu,R_MPFjet1,eweight);
          hprof2D_ZpT_Mu_RMPFjetn[i]->Fill(Zboson.Pt(),mu,R_MPFjetn,eweight);
          hprof2D_ZpT_Mu_RMPFuncl[i]->Fill(Zboson.Pt(),mu,R_MPFuncl,eweight);
    """

def pileup_reweight(events,year):
    weights = coffea.analysis_tools.Weights(len(events),storeIndividual=True)
    
